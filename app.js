/**
 * INCLUDE LIBRARY
 */

const path = require('path');
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('./config'); // get our config file
const app = express();
const User = require('./server/models').User;
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);
const cors = require('cors')
const builder = require('xmlbuilder');
const xml = require('xml');
var http = require("http");
/**
 * 
 * KONFIGURASI 
 */
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('superSecret', config.secret); // secret variable
app.disable('x-powered-by');


app.use(express.static(path.join(__dirname, 'public')));
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});


/**
 * 
 * AUTH ENTYPOINT
 * GENERATE JWT TOKEN
 */
app.post('/auth', function(req, res) {
    console.log('auth');
    // find the user
    User.find({ where: { username: req.body.username, deleted: false } })
        .then(user => {
            console.log(JSON.stringify(user));
            if (!user) {
                res.json({ success: false, message: 'Authentication failed. User not found.' });
            } else {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    var data = {
                        id: user.id,
                        username: user.username,
                        email: user.email,
                        level: user.level,
                        data: user.data,
                    }
                    let token = jwt.sign(data, app.get('superSecret'), {
                        expiresIn: '24h'
                    });
                    res.status(200).json({
                        success: true,
                        role: user.level,
                        token: token
                    });

                } else {
                    res.status(401).json({ success: false, message: "Username / Password Tidak ditemukan" });
                }
            }
        })
        .catch(err => {
            res.status(401).send("Login dulu ke /auth");
        })
});




/**
 * MINDDLEWARE JWT TOKEN
 * 
 */
app.use(function(req, res, next) {

    var token = req.body.token || req.query.token || req.headers['authorization'];
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {
            if (err) {
                return res.status(401).json({ success: false, message: 'UNAUTHORIZATION REQUEST' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(401).send({
            success: false,
            message: 'UNAUTHORIZATION REQUEST'
        });

    }
});




/**
 * INCLUDE ROUTE 
 */
require('./server/routes')(app);
app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to the beginning of nothingness.',
}));


/**
 * 
 * EXPORT MODULE
 */
module.exports = app;
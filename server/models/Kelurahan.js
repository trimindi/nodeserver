module.exports = (sequelize, DataTypes) => {
    const Kelurahan = sequelize.define('Kelurahan', {
        id_kel: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true

        },
        nama_kelurahan: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        id_kec: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        kodepos: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        longitude: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lattitude: {
            type: DataTypes.STRING,
            allowNull: false

        }
    }, {
        tableName: 'dt_kelurahan',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Kelurahan;
};
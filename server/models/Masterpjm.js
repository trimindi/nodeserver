module.exports = (sequelize, DataTypes) => {
    const Masterpjm = sequelize.define('Masterpjm', {
        NOPINJ: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        IDLOS: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACC: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KD_SBR: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        PB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        GP: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        BGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PROV: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ADMIN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        CPP: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        JW: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        TGLAKAD: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLBEBAN: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLANGS: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        JATUHTEMPO: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        PLAFOND: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        OUTSTANDING: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        PENAMBAHAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ANGSPOKOK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ANGSBUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ANGSURAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        SBP: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        KE: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        NOMANGSPOKOK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMANGSBUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMANGSURAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMDENDA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMPENALTY: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMANGSPOKOK_CONVR: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMANGSBUNGA_CONVR: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMANGSURAN_CONVR: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        AG: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        AGR: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        RESUME: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KOMENTAR: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        tableName: 'masterpjm',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Masterpjm.removeAttribute('id');
    return Masterpjm;
};
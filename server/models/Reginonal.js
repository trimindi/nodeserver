module.exports = (sequelize, DataTypes) => {
    const Regional = sequelize.define('Regional', {
        idreg: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        nama_regional: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nama_koordinator: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        telepon: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        e_mail: {
            type: DataTypes.STRING,
            allowNull: false,
        }

    }, {
        tableName: 'dt_regional',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Regional.removeAttribute('id');
    return Regional;
};
module.exports = (sequelize, DataTypes) => {
    const Mastersimp = sequelize.define('Mastersimp', {
        NOSIMP: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ACC: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIB: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        DEBET: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        KREDIT: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        BLOKIR: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        SALDO: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        TGLBUKA: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLAKTIF: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLBEBAN: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLSETOR: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLSETORSBL: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        },

    }, {
        tableName: 'mastersimp',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Mastersimp.removeAttribute('id');
    return Mastersimp;
};
module.exports = (sequelize, DataTypes) => {
    const Cif = sequelize.define('Cif', {
        cif: {
            type: DataTypes.STRING,
            allowNull: false,
            primayKey: true
        },
        nama_client: {
            type: DataTypes.STRING,
            allowNull: false,

        },
        no_spk: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        akta: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        alamat: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        longitude: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lattitude: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        telepon: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        fax: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        website: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        idcab: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        id_prop: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        id_kab: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        id_kec: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        id_kel: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        kode_pos: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nama_ketua: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        telepon_ketua: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nama_wakil_ketua: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        telepon_wakil_ketua: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nama_bendahara: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        telepon_bendahara: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nama_pic: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        telepon_pic: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email_pic: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        anggota: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        ip_address: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        tgl_cutoff: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        bln_cutoff: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        thn_cutoff: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        tgl_reg: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        tgl_apv: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        tgl_act: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        tgl_pbl: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        key_registrasi: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        st: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        }
    }, {
        tableName: 'cif',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Cif.removeAttribute('id');
    return Cif;
};
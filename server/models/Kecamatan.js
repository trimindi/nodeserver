module.exports = (sequelize, DataTypes) => {
    const Kecamatan = sequelize.define('Kecamatan', {
        id_kec: {
            type: DataTypes.STRING,
            allowNull: false,
            primayKey: true
        },
        id_kab: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nama_kecamatan: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        kodepos: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ket: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        tableName: 'dt_kecamatan',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Kecamatan.removeAttribute('id');
    return Kecamatan;
};
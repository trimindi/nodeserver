module.exports = (sequelize, DataTypes) => {
    const Propinsi = sequelize.define('Propinsi', {
        idreg: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true

        },
        id_prop: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nama_propinsi: {
            type: DataTypes.STRING,
            allowNull: false,
        }

    }, {
        tableName: 'dt_propinsi',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Propinsi;
};
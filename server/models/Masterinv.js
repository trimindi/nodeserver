module.exports = (sequelize, DataTypes) => {
    const Masterinv = sequelize.define('Masterinv', {
        KODE: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NAMA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACC: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TGL_BELI: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGL_SUSUT: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        SATUAN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        HARGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        JML: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        PEROLEHAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        UMUR: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        PB: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        KE: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        SISA: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        BEBAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PENYUSUTAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NBUKU: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        tableName: 'inventaris',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Masterinv.removeAttribute('id');
    return Masterinv;
};
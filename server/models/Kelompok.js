module.exports = (sequelize, DataTypes) => {
    const Kelompok = sequelize.define('Kelompok', {
        accjenis: {
            type: DataTypes.STRING,
            allowNull: false,

        },
        acckel: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        kelompok: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        gol: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        subgol: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        cif: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        }
    }, {
        tableName: 'dt_acckel',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Kelompok;
};
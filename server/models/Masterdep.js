module.exports = (sequelize, DataTypes) => {
    const Masterdep = sequelize.define('Masterdep', {
        ACC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NODEP: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        BILYET: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TGLBUKA: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLAKTIF: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        TGLCAIR: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        REKST: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        NOSIMP: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        JW: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PB: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        RO: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        BGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        BUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PJK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PAJAK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PEN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PENALTY: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMINAL: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }

    }, {
        tableName: 'masterdep',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Masterdep.removeAttribute('id');
    return Masterdep;
};
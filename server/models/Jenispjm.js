module.exports = (sequelize, DataTypes) => {
    const Jenispjm = sequelize.define('Jenispjm', {
        ACC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        TBUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        BUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PBUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PTITIPAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        BGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        TTP: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PROV: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ADMIN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        CPP: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        DENDA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ACCBGA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCTTP: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPROV: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCADMIN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCCPP: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCDENDA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPEN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        PB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }

    }, {
        tableName: 'jenispjm',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Jenispjm.removeAttribute('id');
    return Jenispjm;
};
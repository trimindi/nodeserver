module.exports = (sequelize, DataTypes) => {
    const Jenissimp = sequelize.define('Jenissimp', {
        ACC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        BGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PJK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        SETAWAL: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        SETMIN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        MINTARIK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        MAXTARIK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        SALMIN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        POT: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        SETORAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ADM: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ACCBGA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPJK: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCADM: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPOT: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        PB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KST: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        },

    }, {
        tableName: 'jenissimp',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Jenissimp.removeAttribute('id');
    return Jenissimp;
};
module.exports = (sequelize, DataTypes) => {
    const Jenisdep = sequelize.define('Jenisdep', {
        ACC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        BGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },

        PJK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ACCBGA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPJK: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCDENDA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPEN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        PEN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        tableName: 'jenisdep',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Jenisdep.removeAttribute('id');
    return Jenisdep;
};
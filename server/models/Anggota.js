module.exports = (sequelize, DataTypes) => {
    const Anggota = sequelize.define('Anggota', {

        CIB: {
            type: DataTypes.STRING,
            allowNull: false
        },
        NOKARTU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        NIP: {
            type: DataTypes.STRING,
            allowNull: true
        },
        NAMA: {
            type: DataTypes.STRING,
            allowNull: true
        },
        KI: {
            type: DataTypes.STRING,
            allowNull: true
        },
        PANGGILAN: {
            type: DataTypes.STRING,
            allowNull: true
        },
        KL: {
            type: DataTypes.STRING,
            allowNull: true
        },
        TMP_LAHIR: {
            type: DataTypes.STRING,
            allowNull: true
        },
        TGL_LAHIR: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ID: {
            type: DataTypes.STRING,
            allowNull: true
        },
        NO_ID: {
            type: DataTypes.STRING,
            allowNull: true
        },
        TGLEXPIRED: {
            type: DataTypes.STRING,
            allowNull: true
        },
        AGAMA: {
            type: DataTypes.STRING,
            allowNull: true
        },
        PENDIDIKAN: {
            type: DataTypes.STRING,
            allowNull: true
        },
        PEKERJAAN: {
            type: DataTypes.STRING,
            allowNull: true
        },
        JABATAN: {
            type: DataTypes.STRING,
            allowNull: true
        },
        GOLONGAN: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ALAMAT: {
            type: DataTypes.STRING,
            allowNull: true
        },
        TELEPON: {
            type: DataTypes.STRING,
            allowNull: true
        },
        HANDPHONE: {
            type: DataTypes.STRING,
            allowNull: true
        },
        E_MAIL: {
            type: DataTypes.STRING,
            allowNull: true
        },
        NAMAAHLIWARIS: {
            type: DataTypes.STRING,
            allowNull: true
        },
        HUBKEL: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ALAMATAHLIWARIS: {
            type: DataTypes.STRING,
            allowNull: true
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }

    }, {
        tableName: 'cib',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Anggota.removeAttribute('id');
    return Anggota;
};
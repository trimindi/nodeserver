module.exports = (sequelize, DataTypes) => {
    const AccountBB = sequelize.define('AccountBB', {
        acckel: {
            type: DataTypes.STRING,
            allowNull: false,

        },
        accbb: {
            type: DataTypes.STRING,
            allowNull: false,

        },
        bukubesar: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        kategori: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        golongan: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        resiko: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        cif: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        }
    }, {
        tableName: 'accountbb',

        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return AccountBB;
};
module.exports = (sequelize, DataTypes) => {
    const Kodepos = sequelize.define('Kodepos', {
        id_kode: {
            type: DataTypes.STRING,
            allowNull: false,
            primayKey: true
        },
        id_kel: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        nama_kelurahan: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        nama_kecamatan: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        res: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        nama_kabupaten: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        nama_propinsi: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        tableName: 'dt_kodepos',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });

    Kodepos.removeAttribute('id');
    return Kodepos;
};
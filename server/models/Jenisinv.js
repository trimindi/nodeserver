module.exports = (sequelize, DataTypes) => {
    const Jenisinv = sequelize.define('Jenisinv', {
        ACC_AKUM: {
            type: DataTypes.STRING,
            allowNull: false
        },
        ACC: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ACC_BIAYA: {
            type: DataTypes.STRING,
            allowNull: true
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true
        },
        TANGGAL: {
            type: DataTypes.STRING,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: false
        },
    }, {
        tableName: 'inventarisgol',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Jenisinv.removeAttribute('id');
    return Jenisinv;
};
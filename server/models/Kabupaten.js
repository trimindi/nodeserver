module.exports = (sequelize, DataTypes) => {
    const Kabupaten = sequelize.define('Kabupaten', {
        id_prop: {
            type: DataTypes.STRING,
            allowNull: true
        },
        id_kab: {
            type: DataTypes.STRING,
            allowNull: false,
            primayKey: true
        },
        res: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        nama_kabupaten: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ibukota: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        tableName: 'dt_kabupaten',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Kabupaten.removeAttribute('id');
    return Kabupaten;
};
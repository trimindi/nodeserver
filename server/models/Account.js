module.exports = (sequelize, DataTypes) => {
    const Account = sequelize.define('Account', {
        accbb: {
            type: DataTypes.STRING,
            allowNull: false,

        },
        acc: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        keterangan: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        golongan: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ku: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        cif: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    }, {
        tableName: 'account',

        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Account.removeAttribute('id');
    return Account;
};
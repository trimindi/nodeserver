module.exports = (sequelize, DataTypes) => {
    const Jenistab = sequelize.define('Jenistab', {
        ACC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        BGA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        PJK: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        SETAWAL: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        SETMIN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        MINTARIK: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        MAXTARIK: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        SALMIN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        POT: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ADM: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCBGA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPJK: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCADM: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPOT: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.STRING,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    }, {
        tableName: 'jenistab',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Jenistab.removeAttribute('id');
    return Jenistab;
};
module.exports = (sequelize, DataTypes) => {
    const Jenishut = sequelize.define('Jenishut', {
        ACC: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCBGA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPROV: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCADMIN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCDENDA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCPENALTY: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        tableName: 'jenishut',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Jenishut.removeAttribute('id');
    return Jenishut;
};
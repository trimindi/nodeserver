module.exports = (sequelize, DataTypes) => {
    const Jenis = sequelize.define('Jenis', {
        accjenis: {
            type: DataTypes.STRING,
            allowNull: false,

        },
        jenis: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        cif: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        }
    }, {
        tableName: 'dt_accjenis',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Jenis;
};
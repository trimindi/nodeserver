module.exports = (sequelize, DataTypes) => {
    const Masterhut = sequelize.define('Masterhut', {
        NOHUT: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ACC: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KKRD: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        NOKONTRAK: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        PB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        GP: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        BGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PROV: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ADMIN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        LAIN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        JW: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        TGLAKAD: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        TGLBEBAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        TGLANGS: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        JATUHTEMPO: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        PLAFOND: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        OUTSTANDING: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ANGSPOKOK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ANGSBUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        ANGSURAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        SBP: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        KE: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        NOMANGSPOKOK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMANGSBUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMANGSURAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMDENDA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        NOMPENALTY: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        TOTANGSPOKOK: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        TOTANGSBUNGA: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        TOTANGSURAN: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        CATATAN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        }

    }, {
        tableName: 'masterhut',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Masterhut.removeAttribute('id');
    return Masterhut;
};
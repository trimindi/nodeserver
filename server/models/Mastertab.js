module.exports = (sequelize, DataTypes) => {
    const Mastertab = sequelize.define('Mastertab', {
        NOTAB: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ACC: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TGLBUKA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        DEBET: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KREDIT: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        SALDO: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        JASA: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.STRING,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    }, {
        tableName: 'mastertab',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Mastertab.removeAttribute('id');
    return Mastertab;
};
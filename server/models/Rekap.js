module.exports = (sequelize, DataTypes) => {
    const Rekap = sequelize.define('Rekap', {
        ACC: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ACCBB: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        KETERANGAN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        DEBET: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        KREDIT: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        KU: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        TANGGAL: {
            type: DataTypes.STRING,
            defaultValue: sequelize.NOW
        },
        BLN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        THN: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        CIF: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    }, {
        tableName: 'rekap',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Rekap.removeAttribute('id');
    return Rekap;
};
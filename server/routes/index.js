const account = require('../controllers').accountController;
const accountbb = require('../controllers').accountbbController;
const jenis = require('../controllers').jenisController;
const kelompok = require('../controllers').kelompokController;
const user = require('../controllers').userController;
const kelurahan = require('../controllers').kelurahanController;
const kecamatan = require('../controllers').kecamatanController;
const kabupaten = require('../controllers').kabupatenController;
const regional = require('../controllers').regionalController;
const kodepos = require('../controllers').kodeposController;
const propinsi = require('../controllers').propinsiController;

const simpanan = require('../controllers').simpanan;
const hutang = require('../controllers').hutang;
const pinjaman = require('../controllers').pinjaman;
const deposito = require('../controllers').deposito;
const inventaris = require('../controllers').inventaris;
const tabungan = require('../controllers').tabungan;
const jenissimpanan = require('../controllers').jenistabungan;
const jenishutang = require('../controllers').jenishutang;
const jenispinjaman = require('../controllers').jenispinjaman;
const jenisdeposito = require('../controllers').jenisdeposito;
const jenisinventaris = require('../controllers').jenisinventaris;
const jenistabungan = require('../controllers').jenistabungan;
const rekap = require('../controllers').rekap;
const anggota = require('../controllers').anggota;
const accountMaster = require('../controllers').account;


const cif = require('../controllers').cifController;


module.exports = (app) => {
    app.get('/api', (req, res) => res.status(200).send({
        message: 'Welcom montoring koperasi',
    }));


    /**
     * ACCOUNT
     */
    app.get('/api/account/', account.list);
    app.post('/api/account/', account.create);
    app.delete('/api/account/', account.destroy);
    app.put('/api/account/', account.update);
    /**
     * ACCOUNT BUKU BESAR
     */
    app.get('/api/accountbb/', accountbb.list);
    app.post('/api/accountbb/', accountbb.create);
    app.post('/api/accountbb/destroy', accountbb.destroy);
    app.put('/api/accountbb/', accountbb.update);

    /**
     *  JENIS
     */
    app.get('/api/jenis/', jenis.list);
    app.post('/api/jenis/', jenis.create);
    app.post('/api/jenis/destroy', jenis.destroy);
    app.put('/api/jenis/', jenis.update);

    /**
     *  KELOMPOK
     */
    app.get('/api/kelompok/', kelompok.list);
    app.post('/api/kelompok/', kelompok.create);
    app.post('/api/kelompok/destroy', kelompok.destroy);
    app.put('/api/kelompok/', kelompok.update);



    /**
     * WILAYAH KELURAHAN
     */
    app.get('/api/wilayah/kelurahan/', kelurahan.list);
    app.get('/api/wilayah/kelurahan/:id', kelurahan.find);
    app.post('/api/wilayah/kelurahan/', kelurahan.create);
    app.post('/api/wilayah/kelurahan/destroy', kelurahan.destroy);
    app.put('/api/wilayah/kelurahan/', kelurahan.update);

    /**
     * WILAYAH KABUPATEN
     */
    app.get('/api/wilayah/kabupaten/', kabupaten.list);
    app.get('/api/wilayah/kabupaten/:id', kabupaten.find);
    app.post('/api/wilayah/kabupaten/', kabupaten.create);
    app.post('/api/wilayah/kabupaten/destroy', kabupaten.destroy);
    app.put('/api/wilayah/kabupaten/', kabupaten.update);

    /**
     * WILAYAH PROPINSI
     */
    app.get('/api/wilayah/propinsi/', propinsi.list);
    app.get('/api/wilayah/propinsi/:id', propinsi.find);
    app.post('/api/wilayah/propinsi/', propinsi.create);
    app.post('/api/wilayah/propinsi/destroy', propinsi.destroy);
    app.put('/api/wilayah/propinsi/', propinsi.update);

    /**
     * WILAYAH KECAMATAN
     */
    app.get('/api/wilayah/kecamatan/', kecamatan.list);
    app.get('/api/wilayah/kecamatan/:id', kecamatan.find);
    app.post('/api/wilayah/kecamatan/', kecamatan.create);
    app.post('/api/wilayah/kecamatan/destroy', kecamatan.destroy);
    app.put('/api/wilayah/kecamatan/', kecamatan.update);


    /**
     * WILAYAH REGIONAL
     */
    app.get('/api/wilayah/regional/', regional.list);
    app.get('/api/wilayah/regional/:id', regional.find);
    app.post('/api/wilayah/regional/', regional.create);
    app.post('/api/wilayah/regional/destroy', regional.destroy);
    app.put('/api/wilayah/regional/', regional.update);

    /**
     * WILAYAH KODE POS
     */
    app.get('/api/wilayah/kodepos/', kodepos.list);
    app.get('/api/wilayah/kodepos/:id', kodepos.find);
    app.post('/api/wilayah/kodepos/', kodepos.create);
    app.post('/api/wilayah/kodepos/destroy', kodepos.destroy);
    app.put('/api/wilayah/kodepos/', kodepos.update);

    /**
     * WILAYAH CIF
     */
    app.get('/api/cif/', cif.list);
    app.get('/api/cif/:id', cif.find);
    app.post('/api/cif/', cif.create);
    app.post('/api/cif/destroy', cif.destroy);
    app.put('/api/cif/', cif.update);


    /**
     * jenis simpanan
     */
    app.post('/api/instance/jenis/deposito', jenisdeposito.create);
    app.post('/api/instance/jenis/simpanan', jenissimpanan.create);
    app.post('/api/instance/jenis/piutang', jenispinjaman.create);
    app.post('/api/instance/jenis/hutang', jenishutang.create);
    app.post('/api/instance/jenis/tabungan', jenistabungan.create);
    app.post('/api/instance/jenis/inventaris', jenisinventaris.create);

    app.post('/api/instance/master/account', accountMaster.create);
    app.post('/api/instance/master/anggota', anggota.create);

    app.post('/api/instance/master/deposito', deposito.create);
    app.post('/api/instance/master/simpanan', simpanan.create);
    app.post('/api/instance/master/piutang', pinjaman.create);
    app.post('/api/instance/master/hutang', hutang.create);
    app.post('/api/instance/master/tabungan', tabungan.create);
    app.post('/api/instance/master/inventaris', inventaris.create);
    app.post('/api/instance/master/rekap', rekap.create);




    /**
     *  USER
     */
    app.get('/api/user/', user.list);
    app.post('/api/user/', user.create);
    app.post('/api/user/destroy', user.destroy);
    app.put('/api/user/', user.update);

};
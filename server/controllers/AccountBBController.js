/**
 * CONTROLLER ACCOUNT BUKU BESAR
 */


const AccountBB = require('../models').AccountBB;

module.exports = {
    /**
     * 
     * CREATE BUKU BESAR
     */
    create(req, res) {
        console.log(req.body.cif);
        return AccountBB
            .create({
                acckel: req.body.acckel,
                accbb: req.body.accbb,
                bukubesar: req.body.bukubesar,
                kategori: req.body.kategori,
                golongan: req.body.golongan,
                resiko: req.body.resiko,
                cif: req.body.cif
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },
    /**
     * LOAD BUKU BESAR
     */
    list(req, res) {
        return AccountBB
            .findAll({ where: { cif: req.decoded.data } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },


    /**
     * HAPUS BUKU BESAR
     */
    destroy(req, res) {
        return AccountBB
            .destroy({ where: { cif: req.body.cif, accbb: req.body.accbb } }).then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },


    /**
     * UPDATE BUKU BESAR
     */
    update(req, res) {
        return AccountBB
            .update({
                acckel: req.body.acckel,
                accbb: req.body.accbb,
                bukubesar: req.body.bukubesar,
                kategori: req.body.kategori,
                golongan: req.body.golongan,
                resiko: req.body.resiko
            }, { where: { cif: req.body.cif, accbb: req.body.accbb } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
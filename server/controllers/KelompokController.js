const Kelompok = require('../models').Kelompok;

module.exports = {
    create(req, res) {
        return Kelompok
            .create({
                accjenis: req.body.accjenis,
                acckel: req.body.acckel,
                kelompok: req.body.kelompok,
                gol: req.body.gol,
                subgol: req.body.subgol,
                cif: req.body.cif
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return Kelompok
            .findAll({ where: { cif: req.decoded.data } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return Kelompok
            .destroy({ where: { cif: req.body.cif, acckel: req.body.acckel } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Kelompok
            .update({
                accjenis: req.body.accjenis,
                acckel: req.body.acckel,
                kelompok: req.body.kelompok,
                gol: req.body.gol,
                subgol: req.body.subgol
            }, { where: { cif: req.body.cif, acckel: req.body.acckel } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
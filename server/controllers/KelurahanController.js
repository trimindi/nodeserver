const Kelurahan = require('../models').Kelurahan;



module.exports = {
    create(req, res) {
        return Kelurahan
            .create({
                id_kel: req.body.id_kel,
                nama_kelurahan: req.body.nama_kelurahan,
                id_kec: req.body.id_kec,
                kodepos: req.body.kodepos,
                longitude: req.body.longitude,
                lattitude: req.body.lattitude
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return Kelurahan
            .findAll()
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },
    find(req, res) {
        return Kelurahan
            .findAll({ where: { id_kel: req.body.id_kel } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return Kelurahan
            .destroy({ where: { id_kel: req.body.id_kel } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Kelurahan
            .update({

                nama_kelurahan: req.body.nama_kelurahan,
                id_kec: req.body.id_kec,
                kodepos: req.body.kodepos,
                longitude: req.body.longitude,
                lattitude: req.body.lattitude
            }, { where: { id_kel: req.body.id_kel } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
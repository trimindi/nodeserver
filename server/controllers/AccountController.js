const Account = require('../models').Account;

module.exports = {
    create(req, res) {
        return Account
            .create({
                accbb: req.body.accbb,
                acc: req.body.acc,
                keterangan: req.body.keterangan,
                golongan: req.body.golongan,
                ku: req.body.ku,
                cif: req.body.cif
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        console.log(req.decoded);
        return Account
            .findAll({ where: { cif: req.decoded.data } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return Account
            .destroy({ where: { cif: req.body.cif, acc: req.body.acc } }).then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Account.update({
                accbb: req.body.accbb || account.accbb,
                keterangan: req.body.keterangan || keterangan.keterangan,
                golongan: req.body.golongan || account.golongan,
                ku: req.body.ku || account.ku,
            }, { where: { cif: req.body.cif, acc: req.body.acc } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
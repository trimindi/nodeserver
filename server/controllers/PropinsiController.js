const Propinsi = require('../models').Propinsi;

module.exports = {
    create(req, res) {
        return Propinsi
            .create({
                idreg: req.body.idreg,
                id_prop: req.body.id_prop,
                nama_propinsi: req.body.nama_propinsi,
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return Propinsi
            .findAll()
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },
    find(req, res) {
        return Propinsi
            .findAll({ where: { idreg: req.body.idreg } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return Propinsi
            .destroy({ where: { idreg: req.body.idreg } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Propinsi
            .update({
                idreg: req.body.idreg,
                id_prop: req.body.id_prop,
                nama_propinsi: req.body.nama_propinsi,
            }, { where: { idreg: req.body.idreg } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
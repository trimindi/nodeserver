const bcrypt = require('bcrypt');
const User = require('../models').User;

module.exports = {
    create(req, res) {
        return User
            .create({
                username: req.body.username,
                nama: req.body.nama,
                password: bcrypt.hashSync(req.body.password),
                email: req.body.email,
                level: req.body.level,
                data: req.body.data,
                st: true,
                deleted: false
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return User
            .findAll({ where: { deleted: false } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return User
            .update({
                deleted: true
            }, { where: { id: req.params.id } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    },
    update(req, res) {
        return User
            .update({
                username: req.body.username,
                nama: req.body.nama,
                email: req.body.email,
                level: req.body.level,
                data: req.body.data,
                st: req.body.st,
                deleted: req.body.deleted
            }, { where: { id: req.params.id } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
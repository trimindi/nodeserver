const Kecamatan = require('../models').Kecamatan;


module.exports = {
    create(req, res) {
        return Kecamatan
            .create({
                id_kec: req.body.id_kec,
                id_kab: req.body.id_kab,
                nama_kecamatan: req.body.nama_kecamatan,
                kodepos: req.body.kodepos,
                ket: req.body.kodepos
            })
            .then((r) => res.status(201).send(r))
            .catch((err) => res.status(400).send(err));
    },

    list(req, res) {
        return Kecamatan.findAll({ raw: true })
            .then((r) => res.status(200).send(r))
            .catch((err) => res.status(400).send(err));

    },

    find(req, res) {
        return Kecamatan
            .findAll({ where: { id_kec: req.params.id } })
            .then((r) => res.status(200).send(r))
            .catch((err) => res.status(400).send(err));

    },

    destroy(req, res) {
        return Kecamatan
            .destroy({ where: { id_kec: req.body.cif } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(err => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Kecamatan
            .update({
                id_kab: req.body.id_kab,
                nama_kecamatan: req.body.nama_kecamatan,
                kodepos: req.body.kodepos,
                ket: req.body.kodepos
            }, { where: { id_kab: req.body.id_kab } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
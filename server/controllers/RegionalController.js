const Regional = require('../models').Regional;

module.exports = {
    create(req, res) {
        return Regional
            .create({
                idreg: req.body.idreg,
                nama_regional: req.body.nama_regional,
                nama_koordinator: req.body.nama_kordinator,
                telepon: req.body.telepon,
                e_mail: req.body.email
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return Regional
            .findAll()
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },
    find(req, res) {
        return Regional
            .findAll({ where: { idreg: req.body.idreg } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return Regional
            .destroy({ where: { idreg: req.body.idreg } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Regional
            .update({
                nama_regional: req.body.nama_regional,
                nama_koordinator: req.body.nama_kordinator,
                telepon: req.body.telepon,
                e_mail: req.body.email
            }, { where: { idreg: req.body.idreg } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
const Jenis = require('../models').Jenis;

module.exports = {
    create(req, res) {
        return Jenis
            .create({
                accjenis: req.body.accjenis,
                jenis: req.body.jenis,
                cif: req.body.cif
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return Jenis
            .findAll({ where: { cif: req.decoded.data } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return Jenis
            .destroy({ where: { cif: req.body.cif, accjenis: req.body.accjenis } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Jenis
            .update({
                jenis: req.body.jenis || jenis.jenis,
            }, { where: { cif: req.body.cif, accjenis: req.body.accjenis } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
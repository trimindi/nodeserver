const Kodepos = require('../models').Kodepos;

module.exports = {
    create(req, res) {
        return Kodepos
            .create({
                id_kode: req.body.id_kode,
                id_kel: req.body.id_kel,
                nama_kelurahan: req.body.nama_kelurahan,
                nama_kecamatan: req.body.nama_kecamatan,
                nama_kabupaten: req.body.nama_kabupaten,
                res: req.body.res,
                nama_propinsi: req.body.nama_propinsi
            })
            .then((r) => res.status(201).send(r))
            .catch((err) => res.status(400).send(err));
    },

    list(req, res) {
        return Kodepos
            .findAll()
            .then((r) => res.status(200).send(r))
            .catch((err) => res.status(400).send(err));

    },
    find(req, res) {
        return Kodepos
            .findAll({ where: { id_kode: req.params.id } })
            .then((r) => res.status(200).send(r))
            .catch((err) => res.status(400).send(err));
    },

    destroy(req, res) {
        return Kodepos
            .destroy({ where: { id_kode: req.body.id_kode } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(err => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Kodepos
            .update({
                id_kel: req.body.id_kel,
                nama_kelurahan: req.body.nama_kelurahan,
                nama_kecamatan: req.body.nama_kecamatan,
                nama_kabupaten: req.body.nama_kabupaten,
                res: req.body.res,
                nama_propinsi: req.body.nama_propinsi
            }, { where: { id_kode: req.body.id_kode } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
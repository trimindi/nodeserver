/**
 * CONTROLLER ACCOUNT BUKU BESAR
 */
const Jenistab = require('../../models').Jenistab;

module.exports = {
    create(req, res) {
        var date = new Date();
        var BLN = (date.getMonth() < 10 ? "0" : "") + (date.getMonth() + 1);
        var THN = date.getFullYear();
        var CIF = req.decoded.data;
        Jenistab.destroy({ where: { BLN: BLN, THN: THN, CIF: CIF } });
        var numberData = 0;
        var numberSucces = 0;
        var numberFailed = 0;
        for (var a of req.body) {
            a.BLN = BLN;
            a.THN = THN;
            a.CIF = CIF;
            numberData++;
            Jenistab.create(a)
                .then((account) => numberSucces++)
                .catch((error) => numberFailed--);
        }
        res.json({
            success: true,
            data: {
                numberData: numberData,
            }
        });
    },
};
const accountController = require('./AccountController');
const accountbbController = require('./AccountBBController');
const jenisController = require('./JenisController');
const kelompokController = require('./KelompokController');
const userController = require('./UserController');
const propinsiController = require('./PropinsiController');
const kecamatanController = require('./KecamatanController');
const kabupatenController = require('./KabupatenController');
const kodeposController = require('./KodeposController');
const kelurahanController = require('./KelurahanController');
const regionalController = require('./RegionalController');
const cifController = require('./CifController');

const simpanan = require('./instance/simpanan');
const hutang = require('./instance/hutang');
const pinjaman = require('./instance/pinjaman');
const deposito = require('./instance/deposito');
const inventaris = require('./instance/inventaris');
const tabungan = require('./instance/tabungan');

const jenissimpanan = require('./instance/jenissimp');
const jenishutang = require('./instance/jenishutang');
const jenispinjaman = require('./instance/jenispinjaman');
const jenisdeposito = require('./instance/jenisdep');
const jenisinventaris = require('./instance/jenisinvetaris');
const jenistabungan = require('./instance/jenistab');
const rekap = require('./instance/rekap');
const anggota = require('./instance/anggota');
const account = require('./instance/account');

module.exports = {
    account,
    anggota,
    simpanan,
    hutang,
    pinjaman,
    deposito,
    inventaris,
    tabungan,
    jenissimpanan,
    jenishutang,
    jenispinjaman,
    jenisdeposito,
    jenisinventaris,
    jenistabungan,
    rekap,
    accountController,
    accountbbController,
    jenisController,
    kelompokController,
    userController,
    regionalController,
    kelurahanController,
    kodeposController,
    kabupatenController,
    cifController,
    kecamatanController,
    propinsiController
};
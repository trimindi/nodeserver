const Kabupaten = require('../models').Kabupaten;

module.exports = {
    create(req, res) {
        return Kabupaten
            .create({
                id_prop: req.body.id_prop,
                id_kab: req.body.id_kab,
                res: req.body.res,
                nama_kabupaten: req.body.nama_kabupaten,
                ibukota: req.body.ibukota
            })
            .then((r) => res.status(201).send(r))
            .catch((err) => res.status(400).send(err));
    },

    list(req, res) {
        return Kabupaten
            .findAll()
            .then((r) => res.status(200).send(r))
            .catch((err) => res.status(400).send(err));

    },
    find(req, res) {
        return Kabupaten
            .findAll({ where: { id_prop: req.params.id } })
            .then((r) => res.status(200).send(r))
            .catch((err) => res.status(400).send(err));
    },

    destroy(req, res) {
        return Kabupaten
            .destroy({ where: { id_kab: req.body.id_kab } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(err => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Kabupaten
            .update({
                id_prop: req.body.id_prop,
                res: req.body.res,
                nama_kabupaten: req.body.nama_kabupaten,
                ibukota: req.body.ibukota
            }, { where: { id_kab: req.body.id_kab } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};
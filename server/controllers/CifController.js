const Cif = require('../models').Cif;

module.exports = {
    create(req, res) {
        return Cif
            .create({
                cif: req.body.cif,
                nama_client: req.body.nama_client,
                no_spk: req.body.no_spk,
                akta: req.body.akta,
                alamat: req.body.alamat,
                longitude: req.body.longitude,
                lattitude: req.body.lattitude,
                telepon: req.body.telepon,
                fax: req.body.fax,
                email: req.body.email,
                website: req.body.website,
                idcab: req.body.idcab,
                id_prop: req.body.id_prop,
                id_kab: req.body.id_kab,
                id_kec: req.body.id_kec,
                id_kel: req.body.id_kel,
                kode_pos: req.body.kode_pos,
                nama_ketua: req.body.nama_ketua,
                telepon_ketua: req.body.telepon_ketua,
                nama_wakil_ketua: req.body.nama_wakil_ketua,
                telepon_wakil_ketua: req.body.telepon_wakil_ketua,
                nama_bendahara: req.body.nama_bendahara,
                telepon_bendahara: req.body.telepon_bendahara,
                nama_pic: req.body.nama_pic,
                telepon_pic: req.body.telepon_pic,
                email_pic: req.body.email_pic,
                anggota: req.body.anggota,
                ip_address: req.body.ip_address,
                tgl_cutoff: req.body.tgl_cutoff,
                bln_cutoff: req.body.bln_cutoff,
                thn_cutoff: req.body.thn_cutoff,
                tgl_reg: req.body.tgl_reg,
                tgl_apv: req.body.tgl_apv,
                tgl_act: req.body.tgl_act,
                tgl_pbl: req.body.tgl_pbl,
                key_registrasi: req.body.key_registrasi,
                st: req.body.st
            })
            .then((account) => res.status(201).send(account))
            .catch((error) => res.status(400).send(error));
    },

    list(req, res) {
        return Cif
            .findAll()
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },
    find(req, res) {
        return Cif
            .findAll({ where: { cif: req.body.cif } })
            .then((account) => res.status(200).send(account))
            .catch((error) => res.status(400).send(error));

    },

    destroy(req, res) {
        return Cif
            .destroy({ where: { cif: req.body.cif } })
            .then(todo => {
                res.status(200).send({ status: true, msg: 'Sukes Hapus data' })
            }).catch(error => res.status(400).send({ status: false, msg: 'Gagal Hapus Data' }));
    },
    update(req, res) {
        return Cif
            .update({

                nama_client: req.body.nama_client,
                no_spk: req.body.no_spk,
                akta: req.body.akta,
                alamat: req.body.alamat,
                longitude: req.body.longitude,
                lattitude: req.body.lattitude,
                telepon: req.body.telepon,
                fax: req.body.fax,
                email: req.body.email,
                website: req.body.website,
                idcab: req.body.idcab,
                id_prop: req.body.id_prop,
                id_kab: req.body.id_kab,
                id_kec: req.body.id_kec,
                id_kel: req.body.id_kel,
                kode_pos: req.body.kode_pos,
                nama_ketua: req.body.nama_ketua,
                telepon_ketua: req.body.telepon_ketua,
                nama_wakil_ketua: req.body.nama_wakil_ketua,
                telepon_wakil_ketua: req.body.telepon_wakil_ketua,
                nama_bendahara: req.body.nama_bendahara,
                telepon_bendahara: req.body.telepon_bendahara,
                nama_pic: req.body.nama_pic,
                telepon_pic: req.body.telepon_pic,
                email_pic: req.body.email_pic,
                anggota: req.body.anggota,
                ip_address: req.body.ip_address,
                tgl_cutoff: req.body.tgl_cutoff,
                bln_cutoff: req.body.bln_cutoff,
                thn_cutoff: req.body.thn_cutoff,
                tgl_reg: req.body.tgl_reg,
                tgl_apv: req.body.tgl_apv,
                tgl_act: req.body.tgl_act,
                tgl_pbl: req.body.tgl_pbl,
                key_registrasi: req.body.key_registrasi,
                st: req.body.st
            }, { where: { cif: req.body.cif } })
            .then(result => {
                res.status(200).send(result)
            }).catch(err => {
                res.status(400).send(err);
            })
    }
};